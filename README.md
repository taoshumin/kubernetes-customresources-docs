## [架构图](https://haojianxun.github.io/2018/11/10/kubernetes%20CRD%E4%B8%8B%E7%AF%87--%E7%BC%96%E5%86%99%E8%87%AA%E5%AE%9A%E4%B9%89%E6%8E%A7%E5%88%B6%E5%99%A8/)

![image-20211229163138776](./assets/image-20211229163138776.png)



##### 一、[Minikube-安装部署](https://gitlab.com/taoshumin/kubernetes-customresources-docs/-/blob/master/01.%E7%8E%AF%E5%A2%83%E9%83%A8%E7%BD%B2/01.minikube-ubuntu.md)
##### 二、[Code-General-使用文档](https://gitlab.com/taoshumin/kubernetes-customresources-docs/-/blob/master/01.%E7%8E%AF%E5%A2%83%E9%83%A8%E7%BD%B2/03.kube-crd.md)
##### 二、[kubebuilder-使用文档](https://gitlab.com/taoshumin/kubernetes-customresources-docs/-/blob/master/01.%E7%8E%AF%E5%A2%83%E9%83%A8%E7%BD%B2/02.kubebuilder-ubuntu.md)



## MacPro M1 Install Kind

```shell
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
  - role: control-plane
  - role: worker
  - role: worker
------------------
安装：go install sigs.k8s.io/kind@v0.14.0
kind create cluster --config k8s-istio-cluster-kind.yaml
kind get clusters
kind delete clusters kind
```