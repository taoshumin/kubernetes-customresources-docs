# 本机操作远程k8s

## 方法一
- 1.在k8s服务器上开代理kube proxy,然后把配置文件下载下来，修改apiserver地址为代理地址

## 方法二
- 2.在kubeadmin安装时，kubeadm init xxxx
- 加入这个参数 --apiserver-cert-extra-scans=172.16.100.10,47.110.100.20(本机ip)

# minikube 

- https://minikube.sigs.k8s.io/docs/start/

## 查看主机架构

```api
arch 
```

## 安装

```api
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```

## 启动 

```api
minikube start
```

## 取别名

```api
alias kubectl="minikube kubectl --"
```

## install kubectl 

```api
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv kubectl $GOPATH/bin
```

# Operator

![Screenshot from 2021-09-23 22-52-42](Screenshot from 2021-10-23 21-19-37.png)

- https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

## 安装

### 一、安装 kubebuilder

- https://github.com/kubernetes-sigs/kubebuilder

```shell script
$ wget https://github.com/kubernetes-sigs/kubebuilder/releases/download/v3.1.0/kubebuilder_darwin_amd64
$ mv kubebuilder $GOPATH/bin
$ kubebuilder --help
```

### 二、安装 kustomize

- https://github.com/kubernetes-sigs/kustomize

```shell script
$ wget https://github.com/kubernetes-sigs/kustomize/archive/refs/tags/api/v0.10.0.zip
$ sudo mv kustomize $GOPATH/bin/
$ kustomize --help
```

## 创建CRD项目 

- https://book.kubebuilder.io/quick-start.html （kubebuilder使用文档）
- https://lailin.xyz/post/operator-03-kubebuilder-tutorial.html
- https://tangxusc.github.io/blog/2019/05/code-generator%E4%BD%BF%E7%94%A8/ (纯手动创建)

### 一、创建 api

- apiVersion: <group>.<domain>
- apiVersion: myapp.gridsum.com
- kind: Reids

```shell script
$ mkdir kube-operator
$ go mod init
$ kubebuilder init --domain gridsum.com
$ kubebuilder create api --group myapp --version v1 --kind Reids
```

### 二、定义YAML（示例）

```shell script
apiVersion: myapp.gridsum.com/v1
kind: Redis
metadata:
  name: myredis
spec:
  port: 6379 # <v1/redis_type/spec/新增port>
```

## 三、CRD自定义资源字段校验的基本方法

- https://book.kubebuilder.io/reference/markers/crd-validation.html

- redis_types.go (添加校验)

```shell script
//+kubebuilder:validation:Minimum:=81
//+kubebuilder:validation:Maximum:=40000
Port int `json:"port,omitempty"`
```

## 四、CRD安装及使用

- 安装CRD
```api
$ make install
```

- 卸载CRD
```api
make uninstall
```

- 运行控制器

```api
make run 
```

- 查看

```api
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kube-operator$ make install
/home/taoshumin_vendor/workspace/go/src/x6t.io/kube-operator/bin/controller-gen "crd:trivialVersions=true,preserveUnknownFields=false" rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases
/home/taoshumin_vendor/workspace/go/src/x6t.io/kube-operator/bin/kustomize build config/crd | kubectl apply -f -
customresourcedefinition.apiextensions.k8s.io/reids.myapp.my.domain configured
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kube-operator$ kubectl get crd
NAME                    CREATED AT
reids.myapp.my.domain   2021-10-24T07:34:17Z
```

- 运行及测试

```api
$ make run 
$ kubectl apply -f ./test/redis.yaml
```

- 发布到k8s 

```api
构建镜像 
make docker-build
发布
make deploy IMG=<some-register>/<project-name>:tag
```

## webhook 

`想修改验证或者自定义验证，或者比较复杂的验证才使用webhook.`
 
- 创建webhook

创建api:

`$ kubebuilder create api --group myapp --version v1 --kind Reids`

- defaulting: 修改
- programmatic-validation: 验证
```api
kubebuilder create webhook --group myapp --version v1 --kind Reids --defaulting  --programmatic-validation
```
备注: group，version，kind与api保持一致


## 证书管理工具

- https://cert-manager.io/docs/installation/
- https://github.com/jetstack/cert-manager

#### 安装

```api
$ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.4/cert-manager.yaml
$ kubectl get pod -A|grep cert
```

```api
rolebinding.rbac.authorization.k8s.io/cert-manager:leaderelection created
rolebinding.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
service/cert-manager created
service/cert-manager-webhook created
deployment.apps/cert-manager-cainjector created
deployment.apps/cert-manager created
deployment.apps/cert-manager-webhook created
mutatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
validatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kube-operator$ kubectl get pod -A|grep cert
cert-manager   cert-manager-5d74ccd7dd-jsk4q              0/1     ContainerCreating   0             2s
cert-manager   cert-manager-cainjector-84589d595f-rj5g2   0/1     ContainerCreating   0             2s
cert-manager   cert-manager-webhook-666d7c4cc4-tnqv9      0/1     ContainerCreating   0             2s
```

- 修改

```api
修改 config/default/kustomization.yaml
```

## 资源的预删除 Finalizer

- 删除

```api
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kube-operator$ kubectl get cm myapp -o yaml
apiVersion: v1
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"ConfigMap","metadata":{"annotations":{},"finalizers":["kubernetes"],"name":"myapp","namespace":"default"}}
  creationTimestamp: "2021-10-24T10:15:16Z"
  finalizers:
  - kubernetes
  name: myapp
  namespace: default
  resourceVersion: "53026"
  uid: 70cfe465-1459-4613-bdbd-bd711a6f4119
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kube-operator$ kubectl get cm myapp -o yaml
apiVersion: v1
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"ConfigMap","metadata":{"annotations":{},"finalizers":["kubernetes"],"name":"myapp","namespace":"default"}}
  creationTimestamp: "2021-10-24T10:15:16Z"
  deletionGracePeriodSeconds: 0
  deletionTimestamp: "2021-10-24T10:16:12Z"
  finalizers:
  - kubernetes
  name: myapp
  namespace: default
  resourceVersion: "53126"
  uid: 70cfe465-1459-4613-bdbd-bd711a6f4119
```

```api
kubectl patch configmap/myapp --type json --patch='[{"op":"remove","path":"/metadata/finalizers"}]'
```

- 查看crd 

```api
kubectl describe crd reids.myapp.my.domain
```
