/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package helper

import (
	"context"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	v1 "x6t.io/kube-operator/api/v1"
)

func GetRedisPodName(config *v1.Reids) []string {
	//  根据副本数生成pod名称
	podName := make([]string, config.Spec.Num)

	for i := 0; i < config.Spec.Num; i++ {
		podName[i] = fmt.Sprintf("%s-%d", config.Name, i)
	}
	fmt.Println("podName:", podName)
	return podName
}

func IsExist(podname string, config *v1.Reids) bool {
	for _, po := range config.Finalizers {
		if podname == po {
			return true
		}
	}
	return false
}

func IsExistPod(podname string,config *v1.Reids,client client.Client) bool {
	err :=client.Get(context.Background(),types.NamespacedName{
		Namespace: config.Namespace,
		Name:      podname,
	},&corev1.Pod{})
	if err!=nil{
		return false
	}
	return true
}

func CreateRedis(client client.Client, config *v1.Reids, podname string,schema *runtime.Scheme) (string, error) {
	//if IsExist(podname, config) {
	//	return "", nil
	//}
	if IsExistPod(podname,config,client) {
		return "", nil
	}

	newPod := &corev1.Pod{}
	newPod.Name = podname
	newPod.Namespace = config.Namespace

	newPod.Spec.Containers = []corev1.Container{
		{
			Name:            podname,
			Image:           "redis:5-alpine",
			ImagePullPolicy: corev1.PullIfNotPresent,
			Ports: []corev1.ContainerPort{
				{
					ContainerPort: int32(config.Spec.Port),
				},
			},
		},
	}

	// 用来指定资源
	err :=controllerutil.SetControllerReference(config,newPod,schema)
	if err!=nil{
		return "", err
	}
	return podname, client.Create(context.Background(), newPod)
}
