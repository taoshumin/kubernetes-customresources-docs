/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/workqueue"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"x6t.io/kube-operator/helper"

	myappv1 "x6t.io/kube-operator/api/v1"
)

// ReidsReconciler reconciles a Reids object
type ReidsReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=myapp.my.domain,resources=reids,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=myapp.my.domain,resources=reids/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=myapp.my.domain,resources=reids/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Reids object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.8.3/pkg/reconcile
func (r *ReidsReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	redis := &myappv1.Reids{}
	// your logic here
	if err := r.Get(ctx, req.NamespacedName, redis); err != nil {
		return ctrl.Result{}, err
	} else {
		// 正在删除
		if !redis.DeletionTimestamp.IsZero() {
			if err := r.ClearRedis(ctx, redis); err != nil {
				return ctrl.Result{}, err
			}
		}

		// 增加
		podname := helper.GetRedisPodName(redis)
		isModofiy := false
		for _, po := range podname {
			podname, err := helper.CreateRedis(r.Client, redis, po,r.Scheme)
			if err != nil {
				return ctrl.Result{}, err
			}
			if podname == "" {
				continue
			}
			if controllerutil.ContainsFinalizer(redis,po) {
				continue
			}
			redis.Finalizers = append(redis.Finalizers, podname)
			isModofiy = true
		}
		// 收缩副本
		if len(redis.Finalizers) > len(podname) {
			isModofiy = true
			err := r.rmIfSurplus(ctx, podname, redis)
			if err != nil {
				return ctrl.Result{}, err
			}
		}

		// 是否发生了Pod创建，如果没有发生，就没必要更新资源
		if isModofiy {
			if err := r.Client.Update(ctx, redis); err != nil {
				return ctrl.Result{}, nil
			}
		}
		return ctrl.Result{}, nil
	}
}

func (r *ReidsReconciler) rmIfSurplus(ctx context.Context, podname []string, config *myappv1.Reids) error {
	for i := 0; i < len(config.Finalizers)-len(podname); i++ {
		err := r.Client.Delete(ctx, &v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      config.Finalizers[len(podname)+i],
				Namespace: config.Namespace,
			},
		})
		if err != nil {
			return err
		}
	}
	config.Finalizers = podname
	return nil
}

func (r *ReidsReconciler) ClearRedis(ctx context.Context, redis *myappv1.Reids) error {
	podList := redis.Finalizers
	for _, podname := range podList {
		err := r.Client.Delete(ctx, &v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      podname,
				Namespace: redis.Namespace,
			},
		})
		if err != nil {
			return err
		}
	}
	redis.Finalizers = []string{}
	return r.Update(ctx, redis)
}

// kubectl delete pod myredis-0
func (r *ReidsReconciler) podDeleteHandler(event event.DeleteEvent, queue workqueue.RateLimitingInterface) {
	fmt.Println("被删除的对象是:", event.Object.GetName())
	// 方法一： 使用标签
	for _, ref := range event.Object.GetOwnerReferences() {
		if ref.Kind == "Reids" && ref.APIVersion == "myapp.my.domain/v1" {
			queue.Add(reconcile.Request{types.NamespacedName{
				Namespace: event.Object.GetNamespace(),
				Name:       ref.Name,
			}})
			// 执行 Add 会再次出发 Reconcile 方法
		}
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *ReidsReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&myappv1.Reids{}).
		Watches(&source.Kind{Type: &v1.Pod{}}, handler.Funcs{DeleteFunc: r.podDeleteHandler}).
		Complete(r)
}
