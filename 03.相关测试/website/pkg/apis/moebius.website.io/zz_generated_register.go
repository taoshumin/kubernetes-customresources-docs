/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//go:generate execDir=~/go/src/k8s.io/code-generator
//go:generate "${execDir}"/generate-groups.sh all x6t.io/website/pkg/client x6t.io/website/pkg/apis moebius.website.io:v1alpha1 --go-header-file "${execDir}"/hack/boilerplate.go.txt
package moebius

const (
	GroupName = "moebius.website.io"
	Version   = "v1alpha1"
)
