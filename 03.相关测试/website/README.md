# CRD

- [Informer - 源码分析](https://www.luozhiyun.com/archives/391)



### 一、K8s快捷键

```shell
alias llr="ls -ltra"
alias psg="ps -ef|grep"
alias k="kubectl"
alias kg="kubectl get"
alias kaf="kubectl apply -f"
alias kdf="kubectl delete -f"
alias kscp="scp -i /home/edge/.ssh/key"
alias kssh="ssh -i /home/edge/.ssh/key"
alias knt="kubectl -n tenant-4m1p7hi-env-u825fm8"
```