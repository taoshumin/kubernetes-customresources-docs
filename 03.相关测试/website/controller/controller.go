/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	appsinformers "k8s.io/client-go/informers/apps/v1"
	coreinformers "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/apps/v1"
	v1 "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	"time"
	"x6t.io/website/pkg/apis/moebius.website.io/v1alpha1"
	clientset "x6t.io/website/pkg/client/clientset/versioned"
	websiteinformers "x6t.io/website/pkg/client/informers/externalversions/moebius.website.io/v1alpha1"
	wlisters "x6t.io/website/pkg/client/listers/moebius.website.io/v1alpha1"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	v1core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/klog"
	versionedScheme "x6t.io/website/pkg/client/clientset/versioned/scheme"
)

const (
	MessageResourceExists = "Resource already exists"
)

type Controller struct {
	kubeclientset    kubernetes.Interface
	versionclientset clientset.Interface

	servicesLister    v1.ServiceLister         // service lister.
	deploymentsLister listers.DeploymentLister // deployment lister.
	deploymentsSynced cache.InformerSynced     // deployment informer.
	websitesLister    wlisters.WebsiteLister   // website lister.
	websitesSynced    cache.InformerSynced     // website informer. link: http://yangxikun.com/kubernetes/2020/03/05/informer-lister.html

	workqueue workqueue.RateLimitingInterface // link: https://blog.csdn.net/weixin_42663840/article/details/81482553
	recorder  record.EventRecorder            // link: https://www.cnblogs.com/luozhiyun/p/13799901.html
}

func NewController(
	kubeclientset kubernetes.Interface,
	versionclientset clientset.Interface,
	deploymentInformer appsinformers.DeploymentInformer,
	serviceInformer coreinformers.ServiceInformer,
	websiteInformer websiteinformers.WebsiteInformer,
) *Controller {

	// https://ahamoment.cn/docs/k8s-doc/chapter9/code-generation-for-customresources/
	eventBroadcaster := record.NewBroadcaster()                                                                     // 初始化 EventBroadcaster.
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")}) // 接收events,上报events到ApiServer.
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: "Website"})               // 初始化 NewRecorder. link: https://cloud.tencent.com/developer/article/1553944 (simple broadcaster实现)
	// link: https://www.cnblogs.com/luozhiyun/p/13799901.html

	c := &Controller{
		kubeclientset:     kubeclientset,
		versionclientset:  versionclientset,
		deploymentsLister: deploymentInformer.Lister(),
		servicesLister:    serviceInformer.Lister(),
		deploymentsSynced: deploymentInformer.Informer().HasSynced,
		websitesLister:    websiteInformer.Lister(),
		websitesSynced:    websiteInformer.Informer().HasSynced,
		workqueue:         workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Websites"),
		recorder:          recorder,
	}

	websiteInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    c.enqueueWebsite,
		UpdateFunc: func(oldObj, newObj interface{}) {
			c.enqueueWebsite(newObj)
		},
	})

	deploymentInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    c.handleObject,
		UpdateFunc: func(old, new interface{}) {
			newDepl := new.(*appsv1.Deployment)
			oldDepl := old.(*appsv1.Deployment)
			if newDepl.ResourceVersion == oldDepl.ResourceVersion {
				return
			}
			c.handleObject(new)
		},
		DeleteFunc: c.handleObject,
	})

	return c
}

func (c *Controller) handleObject(obj interface{})  {
	var object metav1.Object
	var ok bool

	if object,ok = obj.(metav1.Object);!ok{
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			utilruntime.HandleError(fmt.Errorf("error decoding object, invalid type"))
			return
		}
		object, ok = tombstone.Obj.(metav1.Object)
		if !ok {
			utilruntime.HandleError(fmt.Errorf("error decoding object tombstone, invalid type"))
			return
		}
		klog.V(4).Infof("Recovered deleted object '%s' from tombstone", object.GetName())
	}

	klog.V(4).Infof("Processing object: %s", object.GetName())
	if ownerRef := metav1.GetControllerOf(object); ownerRef != nil {
		// If this object is not owned by a Foo, we should not do anything more
		// with it.
		if ownerRef.Kind != "Website" {
			return
		}

		website, err := c.websitesLister.Websites(object.GetNamespace()).Get(ownerRef.Name)
		if err != nil {
			klog.V(4).Infof("ignoring orphaned object '%s' of foo '%s'", object.GetSelfLink(), ownerRef.Name)
			return
		}

		c.enqueueWebsite(website)
		return
	}
}

func (c *Controller) enqueueWebsite(obj interface{})  {
	var key string
	var err error
	if key,err = cache.MetaNamespaceKeyFunc(obj);err!=nil{
		utilruntime.HandleError(err)
		return
	}
	c.workqueue.Add(key)
}

func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) error {
	defer utilruntime.HandleCrash()
	defer c.workqueue.ShutDown()
	utilruntime.Must(versionedScheme.AddToScheme(scheme.Scheme))

	klog.Info("Starting website controller")
	if ok := cache.WaitForCacheSync(stopCh, c.deploymentsSynced, c.websitesSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	return nil
}

func (c *Controller) runWorker() {
	for c.processNextWorkItem() {
	}
}

func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()
	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			utilruntime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		// Run the syncHandler, passing it the namespace/name string of the
		// Foo resource to be synced.
		if err := c.syncHandler(key); err != nil {
			// Put the item back on the workqueue to handle any transient errors.
			c.workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %s, requeuing", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		klog.Infof("Successfully synced '%s'", key)
		return nil
	}(obj)

	if err != nil {
		utilruntime.HandleError(err)
		return false
	}
	return true
}

func (c *Controller) syncHandler(key string) error {
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return err
	}

	website, err := c.websitesLister.Websites(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			utilruntime.HandleError(fmt.Errorf("err: website '%s' in work queue no longer exists", key))
			return nil
		}
		return err
	}

	deploymentName := website.Spec.DeploymentName
	if deploymentName == "" {
		utilruntime.HandleError(fmt.Errorf("%s: deployment name must be specified", key))
		return nil
	}

	serviceName := fmt.Sprintf("%s-%s", deploymentName, "npsv")
	webSiteService, err := c.servicesLister.Services(website.Namespace).Get(serviceName)
	if err != nil {
		if errors.IsNotFound(err) {
			targetService, err := c.kubeclientset.CoreV1().Services(website.Namespace).Create(context.Background(), newService(website), metav1.CreateOptions{})
			if err != nil {
				return err
			}
			klog.Info("target website service created: %v", targetService)
		}
	}

	klog.Infof("target service found: %v", webSiteService)

	deployment, err := c.deploymentsLister.Deployments(website.Namespace).Get(deploymentName)
	if errors.IsNotFound(err) {
		deployment, err = c.kubeclientset.AppsV1().Deployments(website.Namespace).Create(context.Background(), newDeployment(website), metav1.CreateOptions{})
	}

	if err != nil {
		return err
	}

	if !metav1.IsControlledBy(deployment,website){
		msg := fmt.Sprintf(MessageResourceExists,deployment.Name)
		c.recorder.Event(website,corev1.EventTypeWarning,"ErrResourceExists",msg)
		return fmt.Errorf(msg)
	}

	if website.Spec.Replicas !=nil && *website.Spec.Replicas != *deployment.Spec.Replicas{
		klog.V(4).Infof("replicas: %d, deployment replicas: %d", name, *website.Spec.Replicas, *deployment.Spec.Replicas)
		deployment,err = c.kubeclientset.AppsV1().Deployments(website.Namespace).Update(context.Background(),newDeployment(website),metav1.UpdateOptions{})
	}

	if err!=nil{
		return err
	}

	err = c.updateWebsiteStatus(website,deployment)
	if err!=nil{
		return err
	}

	c.recorder.Event(website,corev1.EventTypeNormal,"Synced","Website synced successfully")
	return nil
}

func (c *Controller)  updateWebsiteStatus(website *v1alpha1.Website, deployment *appsv1.Deployment) error {
	websiteCopy :=website.DeepCopy()
	websiteCopy.Status.AvailableReplicas = deployment.Status.AvailableReplicas

	_,err := c.versionclientset.MoebiusV1alpha1().Websites(website.Namespace).Update(context.Background(),websiteCopy,metav1.UpdateOptions{})
	return err
}

func newDeployment(website *v1alpha1.Website) *appsv1.Deployment {
	labels := map[string]string{
		"app":        "website-nginx",
		"controller": website.Name,
	}
	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      website.Spec.DeploymentName,
			Namespace: website.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(website, v1alpha1.SchemeGroupVersion.WithKind("Website")),
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: website.Spec.Replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: v1core.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: v1core.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "nginx",
							Image: "nginx",
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "html",
									MountPath: "/usr/share/nginx/html",
									ReadOnly:  true,
								},
							},
							Ports: []corev1.ContainerPort{
								{
									ContainerPort: 80,
									Protocol:      "TCP",
								},
							},
						},
						{
							Name:  "git-sync",
							Image: "openweb/git-sync",
							Env: []corev1.EnvVar{
								{
									Name:  "GIT_SYNC_REPO",
									Value: website.Spec.GitRepo,
								},
								{
									Name:  "GIT_SYNC_DEST",
									Value: "/gitrepo",
								},
								{
									Name:  "GIT_SYNC_BRANCH",
									Value: "master",
								},
								{
									Name:  "GIT_SYNC_REV",
									Value: "FETCH_HEAD",
								},
								{
									Name:  "GIT_SYNC_WAIT",
									Value: "3600",
								},
							},
						},
					},
					Volumes: []corev1.Volume{
						{
							Name: "html",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{
									Medium: "",
								},
							},
						},
					},
				},
			},
		},
	}
}

func newService(website *v1alpha1.Website) *v1core.Service {
	deploymentName := website.Spec.DeploymentName
	serviceName := fmt.Sprintf("%s-%s", deploymentName, "npsvc")
	labels := map[string]string{
		"app":        "website",
		"controller": website.Name,
	}
	selectorLabels := map[string]string{
		"app":        "website-nginx",
		"controller": website.Name,
	}
	return &v1core.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceName,
			Namespace: website.Namespace,
			Labels:    labels,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(website, v1alpha1.SchemeGroupVersion.WithKind("Website")),
			},
		},
		Spec: v1core.ServiceSpec{
			Ports: []v1core.ServicePort{
				{
					Port:     80,
					Protocol: v1core.ProtocolTCP,
				},
			},
			Type:     v1core.ServiceTypeNodePort,
			Selector: selectorLabels,
		},
	}
}
