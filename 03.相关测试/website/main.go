/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"k8s.io/klog"
	"path/filepath"
	"time"
	"x6t.io/website/controller"
	clientset "x6t.io/website/pkg/client/clientset/versioned"
	informers "x6t.io/website/pkg/client/informers/externalversions"
)

//go:generate controller-gen paths=x6t.io/website/pkg/apis/moebius.website.io/v1alpha1  crd:trivialVersions=true  crd:crdVersions=v1  output:crd:artifacts:config=manifests
func main() {
	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		klog.Info("Building config from flags failed, %s, trying to build inclusterconfig", err.Error())
		config, err = rest.InClusterConfig()
		if err != nil {
			klog.Fatalf("error %s building inclusterconfig", err.Error())
		}
	}
	kubeClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		klog.Fatalf("Error building kubernetes clientset: %s", err.Error())
	}

	versionclientset, err := clientset.NewForConfig(config)
	if err != nil {
		klog.Fatalf("Error building version clientset: %s", err.Error())
	}

	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(kubeClient, time.Second*30)
	versionInformerFactory := informers.NewSharedInformerFactory(versionclientset, time.Second*30)

	c := controller.NewController(
		kubeClient,
		versionclientset,
		kubeInformerFactory.Apps().V1().Deployments(),
		kubeInformerFactory.Core().V1().Services(),
		versionInformerFactory.Moebius().V1alpha1().Websites())

	stopCh := make(chan struct{})

	kubeInformerFactory.Start(stopCh)
	versionInformerFactory.Start(stopCh)

	if err := c.Run(2, stopCh); err != nil {
		klog.Fatalf("Error running controller: %s", err.Error())
	}
}
