# CustomResources

- [Controller-runtime - 生成代码](https://github.com/kubernetes-sigs/controller-runtime)
- [Controller-tools - 生成CRD模板](https://github.com/kubernetes-sigs/controller-tools)
- [api-reference](https://docs.digitalocean.com/reference/api/api-reference/#operation/create_kubernetes_cluster)
- [OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md)

### 一、查看K8s的版本

```shell
taoshumin_vendor@Vostro-3881:~/workspace/go/src/k8s.io/code-generator$ kubectl version
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:38:50Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:32:41Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
```

### 二、安装 code-generator

注意： `选择对应的k8s版本,go.mod版本也一致`

```shell
git clone https://github.com/kubernetes/code-generator.git k8s.io/code-generator
git checkout -b release-1.22 origin/release-1.22
execDir=~/workspace/go/src/k8s.io/code-generator
"${execDir}"/generate-groups.sh all x6t.io/kluster/pkg/client x6t.io/kluster/pkg/apis viveksingh.dev:v1alpha1 --go-header-file "${execDir}"/hack/boilerplate.go.txt
```

### 三、生成CRD

```shell
git clone git clone git@github.com:kubernetes-sigs/controller-tools.git k8s.io/controller-tools
git checkout -b release-0.4 origin/release-0.4
go build -o controller-gen cmd/controller-gen
mv controller-gen $GOPATH/bin/
controller-gen paths=x6t.io/kluster/pkg/apis/viveksingh.dev/v1alpha1  crd:trivialVersions=true  crd:crdVersions=v1  output:crd:artifacts:config=manifests
```

- 生成

```shell
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kluster$ kubectl create -f manifests/viveksingh.dev_klusters.yaml 
customresourcedefinition.apiextensions.k8s.io/klusters.viveksingh.dev created
```

- 查看

```shell
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kluster$ kubectl get crd
NAME                                  CREATED AT
certificaterequests.cert-manager.io   2021-10-24T08:16:54Z
certificates.cert-manager.io          2021-10-24T08:16:54Z
challenges.acme.cert-manager.io       2021-10-24T08:16:54Z
clusterissuers.cert-manager.io        2021-10-24T08:16:54Z
issuers.cert-manager.io               2021-10-24T08:16:55Z
klusters.viveksingh.dev               2021-11-21T07:11:35Z
orders.acme.cert-manager.io           2021-10-24T08:16:55Z
```

- 运行

```shell
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kluster$ go build 
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kluster$ ./kluster 
0
```

- 创建

```shell
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kluster$ kubectl create -f manifests/klsuterone.yaml 
kluster.viveksingh.dev/kluster-0 created
```

- 验证

```shell
taoshumin_vendor@Vostro-3881:~/workspace/go/src/x6t.io/kluster$ kubectl get kluster
NAME        CLUSTERID   PROGRESS
kluster-0  
```