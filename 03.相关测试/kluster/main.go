/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"log"
	"path/filepath"
	"time"
	"x6t.io/kluster/pkg/controller"

	"x6t.io/kluster/pkg/client/clientset/versioned"
	kInfFac "x6t.io/kluster/pkg/client/informers/externalversions"
)

func main() {
	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		log.Printf("Building config from flags failed, %s, trying to build inclusterconfig", err.Error())
		config, err = rest.InClusterConfig()
		if err != nil {
			log.Printf("error %s building inclusterconfig", err.Error())
		}
	}

	klientset, err := versioned.NewForConfig(config)
	if err != nil {
		log.Printf("getting kclient set %s\n", err)
	}

	infoFactory :=	kInfFac.NewSharedInformerFactory(klientset,20*time.Minute)
	ch := make(chan struct{})
	c  :=controller.NewController(klientset,infoFactory.Viveksingh().V1alpha1().Klusters())

	infoFactory.Start(ch)

	if err := c.Run(ch);err !=nil{
		log.Printf("error start run")
	}
}
