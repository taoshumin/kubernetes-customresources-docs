/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"fmt"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
	"log"
	"time"
	klientset "x6t.io/kluster/pkg/client/clientset/versioned"
	kinf "x6t.io/kluster/pkg/client/informers/externalversions/viveksingh.dev/v1alpha1"
	klister "x6t.io/kluster/pkg/client/listers/viveksingh.dev/v1alpha1"
)

type Controller struct {
	// clientset for custom resource kluster.
	klient klientset.Interface
	// klsuter has synced
	klusterSynced cache.InformerSynced
	// lister
	klister klister.KlusterLister
	// queue
	wq workqueue.RateLimitingInterface
}

func NewController(klient klientset.Interface, klusterInformer kinf.KlusterInformer) *Controller {
	c := &Controller{
		klient:        klient,
		klusterSynced: klusterInformer.Informer().HasSynced,
		klister:       klusterInformer.Lister(),
		wq:            workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "kluster"),
	}

	klusterInformer.Informer().AddEventHandler(
		cache.ResourceEventHandlerFuncs{
			AddFunc: c.handleAdd,
			DeleteFunc: c.handleDel,
		},
	)
	return c
}

func (c *Controller) Run(ch chan struct{}) error {
	if ok := cache.WaitForCacheSync(ch,c.klusterSynced);!ok{
		return fmt.Errorf("cache was noyt sycned")
	}

	go wait.Until(c.worker,time.Second,ch)

	<- ch
	return nil
}

func (c *Controller) worker()  {
	for c.processNextItem() {

	}
}

func (c *Controller) processNextItem() bool {
	item,shutdown := c.wq.Get()
	if shutdown{
		return false
	}
	key,err:=cache.MetaNamespaceKeyFunc(item)
	if err!=nil{
		log.Printf("error %s call namespace key func on cache for item",err)
		return false
	}

	ns,name,err:=cache.SplitMetaNamespaceKey(key)
	if err!=nil{
		log.Printf("split key into namespace and name,error %s",err)
		return false
	}

	kluster,err:=c.klister.Klusters(ns).Get(name)
	if err !=nil {
		log.Printf("error %s, get the kluster resource from name and namespace",err)
		return false
	}

	log.Printf("kluster spec %v",kluster.Spec)
	return true
}

func (c *Controller) handleAdd(obj interface{}) {
	fmt.Println("handle add")
	c.wq.Add(obj)
}

func (c *Controller) handleDel(obj interface{})  {
	fmt.Println("handle del")
	c.wq.Add(obj)
}

